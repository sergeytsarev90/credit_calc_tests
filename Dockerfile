FROM python:3.10-slim
WORKDIR /app
COPY . /app

ENV PIPENV_PYPI_MIRROR="https://pypi.org/simple" \
    PIPENV_VENV_IN_PROJECT="true"

RUN pip install pipenv
RUN apt-get update
RUN apt-get -qq -y install zip curl python3-dev

ENV PYTHONPATH=/app
RUN pipenv sync
ENTRYPOINT [ "pipenv", "run", "pytest", "-s", "-v", "tests", "--continue-on-collection-errors" ]
