import json

import pytest as pytest
import requests

from src.config.config import HEADERS, URL
from src.helpers.calc import year_pay


class TestCheckCredit:

    @pytest.mark.parametrize("source_income", ['пассивный доход', 'наёмный работник', 'собственный бизнес'])
    @pytest.mark.parametrize("credit_rate", [-1, 0, 1, 2])
    @pytest.mark.parametrize("credit_sum", [0.1, 0.5])
    @pytest.mark.parametrize("goal", ['ипотека', 'развитие бизнеса', 'автокредит', 'потребительский'])
    def test_check_credit(self, source_income, credit_rate, credit_sum, goal, data_for_credit):
        data = data_for_credit(source_income, credit_rate, credit_sum, goal)
        response = requests.post(URL, data=json.dumps(data), headers=HEADERS)
        assert response.status_code == 200
        assert response.json()['sum'] == year_pay(data)
        assert response.json()['result'] == 'Кредит одобрен'

    @pytest.mark.parametrize(
        "client_data",
        [
            {
                "source_income": 'наёмный работник',
                "credit_sum": 4.5,
                "credit_rate": 0,
            },
            {
                "source_income": 'собственный бизнес',
                "credit_sum": 9.5,
                "credit_rate": 1,
            },
        ]
    )
    def test_check_credit_diff_sum(self, client_data, data_for_credit_diff_sum):
        data = data_for_credit_diff_sum(client_data)

        response = requests.post(URL, data=json.dumps(data), headers=HEADERS)
        assert response.status_code == 200

        assert response.json()['sum'] == year_pay(data)

    @pytest.mark.parametrize(
        "remove_key",
        ["age", "gender", "source_income", "income_last_year", "credit_rate", "credit_sum", "maturity", "goal"]
    )
    def test_check_credit_without_some_values(self, remove_key):
        data = {
            "age": 30,
            "gender": "M",
            "source_income": 'наёмный работник',
            "income_last_year": 5,
            "credit_rate": 2,
            "credit_sum": 1,
            "maturity": 20,
            "goal": 'ипотека'
        }
        del data[remove_key]
        response = requests.post(URL, data=json.dumps(data), headers=HEADERS)
        assert response.status_code == 422

    @pytest.mark.parametrize(
        "not_available_value",
        [
            {"age": -1}, {"gender": 'q'}, {"source_income": '123'}, {"income_last_year": -1}, {"credit_rate": 5},
            {"credit_sum": -2}, {"maturity": -1}, {"goal": '123'}
        ]
    )
    def test_check_credit_not_available_values(self, not_available_value):
        data = {
            "age": 30,
            "gender": "M",
            "source_income": 'наёмный работник',
            "income_last_year": 5,
            "credit_rate": 2,
            "credit_sum": 1,
            "maturity": 20,
            "goal": 'ипотека'
        }
        data.update(not_available_value)
        response = requests.post(URL, data=json.dumps(data), headers=HEADERS)
        assert response.status_code == 404
        assert response.json()['detail'] == f'{list(not_available_value.keys())[0]} field is wrong'

    @pytest.mark.parametrize(
        "not_available_value",
        [
            {"age": 66, "gender": 'M'}, {"age": 61, "gender": 'F'}, {"credit_rate": -2},
            {'credit_sum': 6, 'maturity': 2, 'income_last_year': 1}, {'source_income': 'безработный'}, {
                'credit_sum': 2, 'source_income': 'пассивный доход', 'credit_rate': -1
            }, {'credit_sum': 6, 'source_income': 'наёмный работник', 'credit_rate': 0
                }, {'credit_sum': 11, 'source_income': 'собственный бизнес', 'credit_rate': 2}
        ]
    )
    def test_check_credit_wrong_values(self, not_available_value):
        data = {
            "age": 30,
            "gender": "M",
            "source_income": 'наёмный работник',
            "income_last_year": 5,
            "credit_rate": 2,
            "credit_sum": 1,
            "maturity": 20,
            "goal": 'ипотека'
        }
        data.update(not_available_value)
        response = requests.post(URL, data=json.dumps(data), headers=HEADERS)
        assert response.status_code == 200
        assert response.json()['result'] == 'Кредит не одобрен'
