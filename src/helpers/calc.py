from math import log


def year_pay(credit_data):
    pay_sum = credit_data['credit_sum'] * (
        1 + credit_data['maturity'] * (
            percent_counter(
                credit_data['goal'],
                credit_data['credit_rate'],
                credit_data['credit_sum'],
                credit_data['source_income']
            )
        )
    ) / credit_data['maturity']
    return pay_sum


def percent_counter(type_credit, rating, sum_credit, income_type):
    base_percent = 10.0
    base_percent += type_percent(type_credit)
    base_percent += rating_percent(rating)
    base_percent += income_type_percent(income_type)

    base_percent += -log(sum_credit)

    return base_percent / 100


def income_type_percent(income_type):
    if income_type == 'пассивный доход':
        return 0.5
    if income_type == 'наёмный работник':
        return -0.25
    if income_type == 'собственный бизнес':
        return 0.25


def rating_percent(rating):
    if rating == -1:
        return 1.5
    elif rating == 1:
        return -0.25
    elif rating == 2:
        return -0.75
    else:
        return 0


def type_percent(type_credit):
    if type_credit == 'ипотека':
        return -2
    elif type_credit == 'развитие бизнеса':
        return -0.5
    elif type_credit == 'потребительский':
        return 1.5
    else:
        return 0
