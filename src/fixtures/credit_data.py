import pytest


@pytest.fixture(scope='function')
def data_for_credit():

    def _data_for_credit(source_income, credit_rate, credit_sum, goal):
        return {
            "age": 30,
            "gender": "M",
            "source_income": source_income,
            "income_last_year": 5,
            "credit_rate": credit_rate,
            "credit_sum": credit_sum,
            "maturity": 20,
            "goal": goal
        }

    return _data_for_credit


@pytest.fixture(scope='function')
def data_for_credit_diff_sum():

    def _data_for_credit_diff_sum(client_data):
        data = {"age": 30, "gender": "M", "income_last_year": 5, "maturity": 20, "goal": 'ипотека'}
        data.update(client_data)
        return data

    return _data_for_credit_diff_sum
