import os

from dotenv import load_dotenv

load_dotenv()

PATH = os.getenv('path')
HEADERS = {'accept': 'application/json', 'Content-Type': 'application/json'}
URL = f'http://{PATH}/v1/check_credit_available'
